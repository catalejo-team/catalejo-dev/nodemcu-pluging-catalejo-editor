NODE=v21.7.3
NVM=sh $$NVM_BIN

help: ## Prints help for targets with comments
	@cat $(MAKEFILE_LIST) | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

d: dev
b: build
p: publish

dependencies: ## Actualizar dependencias del proyecto con npm
	npm update
	$(NVM) use $(NODE)

init: ## Instalación de dependencias
	npm install

dev: ## Proceso de desarrollo
	npm run dev

build: ## Construir la dist (no debe existir errores o warnings)
	npm run build

build-only:	## Constrir pasando las advertencias del compilador
	npm run build-only

publish: build ## Publicar en npmjs
	npm publish --access public

PLUG_NAME=nodemcu

pack: build ## Empaquetar en .zip el plugin (dist + lua + docs + libs)
	rm -rf $(PLUG_NAME) # Remover nombre del directorio
	rm -f $(PLUG_NAME).zip
	mkdir -p $(PLUG_NAME)
	cp -var ./dist/* $(PLUG_NAME)
	cp -var ./lua $(PLUG_NAME)
	cp -var ./docs/ $(PLUG_NAME)
	zip -r $(PLUG_NAME).zip $(PLUG_NAME)
	# rm -rf $(PLUG_NAME) # remover directorio de plugin
	ls -ltrh $(PLUG_NAME).zip

clean: ## Limpiar resultados en dist
	rm -rf dist $(PLUG_NAME)*
