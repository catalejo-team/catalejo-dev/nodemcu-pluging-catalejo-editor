import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';
import { fileURLToPath, URL } from 'node:url'

export default defineConfig({
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    lib: {
      entry: 'src/main.ts',
      name: 'nodemcu',
      fileName: (format) => `nodemcu.${format}.js`
    },
    rollupOptions: {
      external: ['@johnnycubides/blockly-catalejo/core'],
      // output: {
      //   globals: {
      //     blockly: 'Blockly'
      //   }
      // }
    }
  },
  plugins: [dts({
    // outDir: 'dist/types',
    outDir: 'dist',
    insertTypesEntry: true
  })],
});
