local serial = require("libserial")

local serialport = {}

-- LOG activa los prints para hacer seguimiento al comportamiento del puerto serial
local LOG = true

local STATUS = {
	OK = 0,
	FAILED = 1,
}

local function read(port, dataSize, timeout)
	-- local LOG = false
	if not timeout then
		timeout = 0
	end
	if not dataSize then
		dataSize = 1
	end
	local data, size = serial.read(port, dataSize or 1, timeout)
	if LOG then
		if size == dataSize then
			print("READ: {" .. data .. "} R_SIZE: {" .. size .. "}")
		elseif size > 0 then
			print("READ: {" .. data .. "} R_SIZE: {" .. size .. "} TIMEOUT!: {" .. timeout .. "}")
		else
			print("READ: DONT RECEIVED")
		end
	end
	return data, size
end

local function write(port, data, timeout)
	-- local LOG = false
	if not timeout then
		timeout = 0
	end
	local result = serial.write(port, data, timeout)
	if LOG then
		if result then
			print("WRITE: {" .. data .. "} W_SIZE: {" .. result .. "}")
		end
	end
	return result
end

local function discardResidualData(port, watchdog)
	-- local LOG = false
	if not watchdog then
		watchdog = 100
	end
	local result = 1
	for i = 1, watchdog do
		_, result = serial.read(port, 256, 50)
		if LOG then
			print("RESIDUAL: " .. result)
		end
		if result <= 0 then
			return STATUS.OK
		end
	end
	return STATUS.FAILED
end

local function readAll(port)
	-- local LOG = false
	local data, result = "", 1
	local block
	while result > 0 do
		block, result = serial.read(port, 256, 50)
		if result > 0 then
			data = data .. block
			if LOG then
				print("READ BLOCK: " .. block)
			end
		end
	end
	if LOG then
		print("READ ALL: " .. data)
	end
	return data
end

local function flush(port, ps_flush)
	-- 1 flush buffer in
	-- 2 flush buffer out
	-- 3 flush both
	serial.flush(port, ps_flush)
end

local function sendData(port, data, separator, timeout)
	-- local LOG = false
	if data then
		for line in data:gmatch("[^" .. separator .. "]+") do
			if LOG then
				print("LINE TO SEND: {" .. line .. "} L_SIZE: {" .. #line .. "}")
			end
			write(port, line .. "\n")
			read(port, #line + 1, timeout)
		end
	end
end

local function sendSetFileName(port, fileName, contentSize, blockSize, timeout)
	local data = 'f, cs ,bs ,b ,c  = "'
		.. fileName
		.. '", '
		.. contentSize
		.. ", "
		.. blockSize
		.. ', "", 0\n'
		.. "on, w, fo, fc, fw, fr = uart.on, uart.write, file.open, file.close, file.write, file.remove\n"
		.. 'if file.exists(f) then fr(f) end fo(f,"w") fw([[""]]) fc() fo(f, "w+")\n'
		.. 'on("data", 0, function(d)\n'
		.. "b = b .. d c = c + 1\n"
		.. 'if #b >= bs -1 then local dw = string.sub(b, 1, bs) b = string.sub(b, bs + 1, #b) fw(dw) w(0, "ok\\n") end\n'
		.. 'if c >= cs then if b ~= "" then fw(b) end  fc() w(0, "fi\\n") on("data") end\n'
		-- .. 'if c >= cs then if b ~= "" then fw(b) end  fc() w(0, "fi\\n") node.restart() end\n'
		.. "end, 0)\n"

	sendData(port, data, "\n", timeout) -- Enviar los datos, en este caso se configura para el envío de un archivo
	readAll(port) -- Si se desea ver la respuesta
	-- discardResidualData(port) -- En caso contraro se decarta la respuesta
end

local function sendFileContent(port, fileContent, blockSize, timeout)
	-- local LOG = false
	local counter = 1
	while true do
		if counter <= #fileContent then
			local block = fileContent:sub(counter, counter + blockSize - 1)
			counter = counter + blockSize
			if LOG then
				print("BLOCK TO SEND: {" .. block .. "}" .. " B_SIZE: {" .. #block .. "}")
			end
			write(port, block) -- Espera hasta enviar todos los bis (blqueante)
			read(port, #"ok\n", timeout) -- Tiempo de espera entre cada envío
		else
			break
		end
	end
end

function serialport.getSerialportList()
	local ports = serial.list_ports()
	if ports then
		return STATUS.OK, ports
	else
		return STATUS.FAILED, "Failed get serialport list"
	end
end

function serialport.saveCode(fileName, content, portName, options, blockSize, timeout)
	local contentSize = #content

	local port, error = serial.open(portName, options) -- se abre el puerto
	if error then
		return STATUS.FAILED, "Don't open port: " .. portName .. ", error: " .. error .. "."
	else
		flush(port, 3) -- Limpiar puerto
		if discardResidualData(port) == STATUS.FAILED then
			serial.close(port) -- Se cierra el puerto
			return STATUS.FAILED, "Don't discard residual data in port: " .. portName .. "."
		end -- Descartar los datos del buffer que no son de utilidad
		read(port, 10, 2000) -- Esperar mientras se reinicia la tarjeta
		if discardResidualData(port) == STATUS.FAILED then
			serial.close(port) -- Se cierra el puerto
			return STATUS.FAILED, "Don't discard residual data in port: " .. portName .. "."
		end -- Descartar los datos del buffer que no son de utilidad
		write(port, "\n", 0) -- Solicitud de sincronización
		local _, data_size = read(port, 10, 100)
		if 1 > data_size then
			serial.close(port) -- Se cierra el puerto
			return STATUS.FAILED, "The serial port: " .. portName .. " don't answer."
		end -- Después de la sincornización se espera que el puerto responda al menos con un dato de entrada
		if discardResidualData(port) == STATUS.FAILED then
			serial.close(port) -- Se cierra el puerto
			return STATUS.FAILED, "Don't discard residual data in port: " .. portName .. "."
		end -- Descartar la anterior respuesta, generalmente es un error
		sendSetFileName(port, fileName, contentSize, blockSize, timeout) -- Se configura el comando para enviar un archivo
		sendFileContent(port, content, blockSize, timeout) -- Se envía el contenido del archivo
		serial.flush(port, 1) -- flush input and output
		read(port, 10, 2000) -- Esperar mientras se reinicia la tarjeta
		if discardResidualData(port) == STATUS.FAILED then
			serial.close(port) -- Se cierra el puerto
			return STATUS.FAILED, "Don't discard residual data in port: " .. portName .. "."
		end -- Descartar la anterior respuesta, generalmente es un error
		write(port, 'dofile("' .. fileName .. '")\n') -- Se ejecuta el archivo que ha sido subido
		readAll(port) -- Si se desea, se puede leer la respuesta
		serial.close(port) -- Se cierra el puerto
		return STATUS.OK, "Save code in nodemcu sucessfull."
	end
end

return serialport
