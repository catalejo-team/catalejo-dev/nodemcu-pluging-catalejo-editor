import { LANG } from "./consts";

const itemsToolBarLang = {
  nodemcuSend: {
    en: "Send",
    es: "Enviar"
  },
  magic: {
    en: "Translate",
    es: "Traducir"
  },
  getSerialportList: {
    en: "Get serial ports",
    es: "Listar puertos seriales"
  },
  docs: {
    en: "Documentation",
    es: "Documentación"
  }
}

const dialogLabelCommands = {
  docs: {
    title: {
      en: "Nodemcu documentation",
      es: "Documentación para Nodemcu"
    },
    description: {
      en: "Open the next link to get information about Nodemcu",
      es: " Abre el siguiente enlace para obtener información sobre Nodemcu"
    }
  }
}

export function getTextFromLang(object: any, lang: LANG): string {
  if (lang == LANG.EN) {
    return object.en;
  } else if (lang == LANG.ES) {
    return object.es;
  } else {
    return "noname";
  }
}

export {
  itemsToolBarLang,
  dialogLabelCommands,
}
