import type { Config, Project, Result, PluginResult, SerialOptions } from "./types";
import * as request from "./http-request";
import { BLOCKLY_EXTENSION, LANG, PLATFORM, STATUS, TEXT_EXTENSION, TYPE_PLUGIN_RESULT } from "./consts";
import { dialogLabelCommands, getTextFromLang } from "./lang";

/** Obtener la información relacionada a la configuración del puertoserial */
function getSerialOptions(config: Config): SerialOptions {
  const serialOptions: SerialOptions = {}
  let atr = config.baudrate;
  if (atr != undefined) {
    serialOptions.baudRate = parseInt(atr);
  }
  atr = config.bits;
  if (atr != undefined) {
    serialOptions.bits = parseInt(atr);
  }
  atr = config.parity;
  if (atr != undefined) {
    serialOptions.parity = parseInt(atr);
  }
  atr = config.stopbits;
  if (atr != undefined) {
    serialOptions.stopBits = parseInt(atr);
  }
  atr = config.flowcontrol;
  if (atr != undefined) {
    serialOptions.flowControl = parseInt(atr);
  }
  atr = config.dtr;
  if (atr != undefined) {
    serialOptions.dtr = parseInt(atr);
  }
  atr = config.rts;
  if (atr != undefined) {
    serialOptions.rts = parseInt(atr);
  }
  atr = config.cts;
  if (atr != undefined) {
    serialOptions.cts = parseInt(atr);
  }
  return serialOptions;
}

/** Enviar archivo a través del puerto serial **/
async function nodemcuSend(project: Project, generateCode: Function): Promise<PluginResult> {

  let portName = "";
  const options: SerialOptions = getSerialOptions(project.config);
  let timeout = 0;
  let fileName = "";
  let content = "";
  let blockSize = 64;

  if (project.config.portname != undefined) {
    portName = project.config.portname;
  } else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "warn",
        summary: "",
        detail: "",
        life: 3000,
      },
    }
    // return { status: STATUS.FAILED, data: "Require portName" };
  }
  if (project.config.timeout != undefined) {
    timeout = parseInt(project.config.timeout);
  }
  // El nombre del archivo no es vacío?
  if (project.filename != undefined && project.filename != "") {
    // Se solicita renombrar el archivo a guardar?
    if (project.config.filerename != undefined && project.config.filerename != "") {
      // Al renombrar tiene la extensión de texto válida por el nodemcu?
      if (project.config.filerename.split(".").pop() == TEXT_EXTENSION) {
        fileName = project.config.filerename;
      } else {
        return {
          typeResult: TYPE_PLUGIN_RESULT.TOAST,
          data: {
            severity: "warn",
            summary: "filerename require extension .lua",
            detail: "",
            life: 3000,
          },
        }
        // return { status: STATUS.FAILED, data: "fileRename require extension .lua" };
      }
    } else {
      // Proviene de un proyecto con extensión de texto válida para nodemcu?
      if (project.extension == TEXT_EXTENSION) {
        // Guardar directamente.
        fileName = project.filename;
      } else {
        // En cualquier otro caso deberá sustuir la extensión por la soportada por esta plataforma
        const index = project.filename.lastIndexOf(".");
        fileName = project.filename.substring(0, index) + "." + TEXT_EXTENSION;
      }
    }
  } else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "warn",
        summary: "Dont have filename",
        detail: "",
        life: 3000,
      },
    }
    // return { status: STATUS.FAILED, data: "Dont have fileName" };
  }
  if (project.content != undefined) {
    // El contenido está en lua?
    if (project.extension == TEXT_EXTENSION) {
      content = project.content;
      // El contenido está en blockly ?
    } else if (project.extension == BLOCKLY_EXTENSION && project.config.platform == PLATFORM) {
      content = generateCode();
    } else {
      return {
        typeResult: TYPE_PLUGIN_RESULT.TOAST,
        data: {
          severity: "warn",
          summary: "Content dont found",
          detail: "",
          life: 3000,
        },
      }
      // return { status: STATUS.FAILED, data: "Content in other format" };
    }
  } else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "warn",
        summary: "Content dont found",
        detail: "",
        life: 3000,
      },
    }
    // return { status: STATUS.FAILED, data: "Content dont found" };
  }
  if (project.config.blocksize != undefined) {
    blockSize = parseInt(project.config.blocksize);
  }
  const result: Result = await request.saveCode(portName, options, timeout, fileName, content, blockSize);
  console.log(result.data);
  if (result.status == STATUS.OK) {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "success",
        summary: "Code save OK",
        detail: result.data,
        life: 3000,
      }
    }
    // return { status: result.status, data: "Save Ok" };
  } else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "error",
        summary: "Failed to saved",
        detail: result.data,
        life: 3000,
      }
    }
    // return { status: result.status, data: "FAILED TO SAVED" };
  }
}

/** Traducir blockly code a lua code */
async function nodemcuMagic(project: Project, generateCode: Function): Promise<PluginResult> {
  if (project.extension == BLOCKLY_EXTENSION) {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TEXT_PRINT,
      data: {
        title: "Lua",
        content: generateCode(),
      }
    };
  } else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "warn",
        summary: "Opción disponible solo con archivos blockly (json)",
        detail: "",
        life: 5000,
      }
    };
  }
}

/** Consultar los puertos seriales disponibles en el sistema **/
async function getSerialPortList(): Promise<PluginResult> {
  const result: Result = await request.getSerialPortList();
  if (result.status == STATUS.OK) {
    // Convertir el resultado de array a un string
    let content = "";
    for (let index = 0; index < result.data.length; index++) {
      content += result.data[index] + "\n";
    }
    return {
      typeResult: TYPE_PLUGIN_RESULT.TEXT_PRINT,
      data: {
        title: "SerialPortList",
        content: content,
      }
    };
  } else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "warn",
        summary: "No se pudo obtener información sobre los puertos seriales",
        detail: "",
        life: 5000,
      }
    };
  }
}

/** Entregar el enlace para visualizar la documentación */
async function getUrlDocs(lang: LANG): Promise<PluginResult> {
  const DOCS = "/plugins/nodemcu/docs/index.html";
  const HOST = "http://" + location.hostname;
  const PORT = 8080;
  // const PORT = location.port;
  const URL = HOST + ":" + PORT + DOCS;
  return {
    typeResult: TYPE_PLUGIN_RESULT.URL_PRINT,
    data: {
      title: getTextFromLang(dialogLabelCommands.docs.title, lang),
      content: URL,
      description: "Abre el siguiente enlace para obtener infomación sobre la documentación de nodemcu",
    }
  }
}

export {
  nodemcuSend,
  nodemcuMagic,
  getSerialPortList,
  getUrlDocs,
}
