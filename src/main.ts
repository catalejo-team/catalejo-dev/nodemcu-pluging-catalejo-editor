import * as nodemcu from '@johnnycubides/nodemcu-blockly-catalejo';
import type { Project, PluginResult } from "./types";
import * as command from "./commands";
import { LANG, PLATFORM, LUA, TYPE_PLUGIN_RESULT, BLOCKLY_TEMPLATE, LANG_TEMPLATE } from "./consts";
import { itemsToolBarLang, getTextFromLang } from "./lang";

/** Al iniciarse  este plugin, debe conservarse las referencia de algunos datos
 * que serán usados por este plugin. */
/** Representación del traductor de blockly a un lenguaje base */
let generator: any;
/** Representación del workspace de blockly */
let workspace: any;
/** Variable de las diferentes operaciones y atributos propios de Blockly */
let Blockly: any;

/** Valor que representa la plataforma para la cual es presentado este plugin */
const platform = PLATFORM;

/** Lenguaje interpretado con el que opera el plugin (indicar la extension en vez del nombre del lenguaje) */
const platformLang = LUA;

/** Template para un nuevo proyecto en blockly */
const blockly_template = BLOCKLY_TEMPLATE;

/** Template para iniciar un proyecto en lenguaje de texto */
const lang_template = LANG_TEMPLATE;

/** Función para agregar a Blockly las funciones específicas de este plugin */
function initBlockly(blocklyRef: any, generatorRef: any, workspaceRef: any) {
  Blockly = blocklyRef;
  generator = generatorRef;
  workspace = workspaceRef;
  nodemcu.addCustomBlocks(Blockly, generator);
}

/** Función para traducir de blockly al lenguaje principal de operación de este plugin **/
function generateCode() {
  return nodemcu.generateCode(generator, workspace);
}

/** Toolbox Blockly de este plugin */
const toolbox = nodemcu.toolbox;

/** Items a ser entregados al menubar de la aplicación Catalejo Editor */
function getItemsToolBarOfPlugin(lang: LANG) {
  return [
    {
      label: getTextFromLang(itemsToolBarLang.magic, lang),
      icon: "pi pi-sparkles",
      command: "nodemcu-magic"
    },
    {
      label: getTextFromLang(itemsToolBarLang.nodemcuSend, lang),
      icon: 'pi pi-send',
      command: "nodemcu-send",
    },
    {
      label: "nodemcu",
      icon: "pi pi-microchip",
      items: [
        {
          label: getTextFromLang(itemsToolBarLang.getSerialportList, lang),
          icon: "pi pi-search",
          command: "nodemcu-getserialportlist"
        },
        {
          label: getTextFromLang(itemsToolBarLang.docs, lang),
          icon: "pi pi-book",
          command: "nodemcu-docs"
        }
      ]

    }
  ]
}

/** Filtro de solicitudes que provienen de la aplicación */
async function commandEmit(project: Project, commandId: string, lang: LANG): Promise<PluginResult> {
  // TODO: Convertir en un swutch
  if (commandId == "nodemcu-send") {
    return await command.nodemcuSend(project, generateCode);
  } else if (commandId == "nodemcu-magic") {
    return await command.nodemcuMagic(project, generateCode);
  } else if (commandId == "nodemcu-getserialportlist") {
    return await command.getSerialPortList();
  } else if (commandId == "nodemcu-docs") {
    return await command.getUrlDocs(lang);
  }
  else {
    return {
      typeResult: TYPE_PLUGIN_RESULT.TOAST,
      data: {
        severity: "error",
        summary: "Función no localizada en el plugin",
        detail: "",
        life: 3000,
      }
    }
  }
}

export {
  platform,
  platformLang,
  toolbox,
  initBlockly,
  generateCode,
  getItemsToolBarOfPlugin,
  commandEmit,
  blockly_template,
  lang_template,
};
