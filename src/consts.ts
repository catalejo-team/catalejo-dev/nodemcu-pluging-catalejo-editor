import type { ProgLang } from "./types";
export enum STATUS {
  OK,
  FAILED,
  CHANGE,
  WITHOUTCHANGE,
  REQUEST, // Solicitud para realizar algo
  REQUEST_SELECT_FILE_IN_TABVIEW,
  EXISTS,
  NOEXIST,
  FOUND,
  NOTFOUND,
  ADDED,
  SAVED,
  UNSAVED,
  SET_WS,
  ENABLED,
  DESABLED,
}

export enum LANG {
  EN,
  ES,
}

/** Tipo de resultados posibles a entrtegar al editor */
export enum TYPE_PLUGIN_RESULT {
  TOAST, TEXT_PRINT, TEXT_AREA, URL_PRINT,
}

export const PYTHON: ProgLang = { name: "Python", extension: "py" };
export const CHUCK: ProgLang = { name: "ChucK", extension: "ck" };
export const LUA: ProgLang = { name: "Lua", extension: "lua" };
export const JAVASCRIPT: ProgLang = { name: "JavaScript", extension: "js" };
export const JSON: ProgLang = { name: "JSON", extension: "json" };

export const PROG_LANG: ProgLang[] = [
  PYTHON,
  CHUCK,
  LUA,
  JAVASCRIPT,
  JSON,
]

export const TEXT_EXTENSION = "lua"

export const BLOCKLY_EXTENSION = "json"

export const PLATFORM = "nodemcu";

export const BLOCKLY_TEMPLATE = "{\"config\":{\"platform\":\"nodemcu\",\"portname\":\"\",\"baudrate\":\"115200\"},\"blocklycode\":\"<xml xmlns=\\\"https://developers.google.com/blockly/xml\\\"></xml>\"}"

export const LANG_TEMPLATE =
`-- platform: nodemcu
-- baudrate: 115200
-- portname: 
`
