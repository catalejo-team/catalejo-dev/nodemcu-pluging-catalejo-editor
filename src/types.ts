import type { STATUS, TYPE_PLUGIN_RESULT } from "@/consts";

// Config es una lista de opciones que se configuran desde el editor.
// Aunque desde el editor es del tipo any y el tamaño no está definido
// Desde el punto de vsita del plugin se restringe las opciones
// de tal maneras que se pueden exponer en la documentación.
export interface Config {
  platform?: string, // Nombre de la plataforma
  portname?: string, // Puerto serial a usar
  timeout?: string, // Timeout del puerto serial
  filerename?: string, // Nombre del archivo a guardar
  blocksize?: string, // Tamaño de los bloques a enviar por serial
  baudrate?: string, // baudRate del puerto serial
  bits?: string, // Bits del puerto serial
  parity?: string, // Paridad del puerto serial
  stopbits?: string, // stopBits del puerto serial
  flowcontrol?: string, // flowControl del puerto serial
  dtr?: string, // dtr del puerto serial
  rts?: string, // rts del puerto serial 
  cts?: string, // cts del puertoserial
}

// Estructura de los proyectos a guardar
export interface Project {
  path: string,
  filename: string,
  extension: string, // la extensión está asociada al lenguaje de codemirror
  content: string,  // Contenido a guardar en el File
  blocklycode: string, // compilación dada por ejemplo desde blockly XML
  codemirrorCursor: number // Posición del cursor
  config: Config,
  status: STATUS, // ADDED, SAVED, UNSAVED
}

// Formato de retorno para funciones
// especialmente como interfaz de vue
export interface Result {
  status: STATUS
  data?: any
}

export interface DataToast {
  severity: string, /** "error" | "success" | "secondary" | "info" | "contrast" | "warn" */
  summary: string,
  detail: string,
  life: number, /** time in ms */
}

export interface DataDialog {
  title: string,
  description?: string,
  content: any,
}

/** Respuesta que debe entregar el plugin para usar en el editor */
export interface PluginResult {
  typeResult: TYPE_PLUGIN_RESULT,
  data?: DataToast | DataDialog,
}

export interface SerialOptions {
  baudRate?: number,
  bits?: number,
  parity?: number,
  stopBits?: number,
  flowControl?: number,
  dtr?: number,
  rts?: number,
  cts?: number,
}


/** Lenguajes de programación */
export interface ProgLang {
  name: string,
  extension: string,
}
