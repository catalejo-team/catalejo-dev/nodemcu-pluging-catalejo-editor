
import { STATUS } from "./consts";
import type { Result, SerialOptions } from "./types";

const HOST = "http://" + location.hostname;
// const PORT = location.port;
const PORT = "8080"
const MODE = "cors" // cors, no-cors, same-origin
const ROUTE_PREFIX = "nodemcu/"

async function fecth(url: string, options: any): Promise<Result> {
  try {
    const response = await fetch(url, options);
    const result = await response.json();
    // TODO: Quitar o modificar, se ha usado para debug.
    if (result.status == STATUS.OK) {
      return result;
    } else {
      return result;
    }
  } catch (error) {
    console.error(error);
    return { status: STATUS.FAILED };
  }
}

export async function saveCode(
  portName: string,
  serialOptions: SerialOptions,
  timeout: number,
  fileName: string,
  content: string,
  blockSize: number
): Promise<Result> {
  // TODO: cambiar el ROUTE que councida con el getDirContent
  const ROUTE = ROUTE_PREFIX + "savecode"
  const url = HOST + ":" + PORT + '/' + ROUTE;

  const body = {
    portName: portName,
    options: serialOptions,
    timeout: timeout,
    fileName: fileName,
    content: content,
    blockSize: blockSize,
  }

  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body)
  };
  return await fecth(url, options);
}

export async function getSerialPortList(): Promise<Result> {
  const ROUTE = ROUTE_PREFIX + "getserialportlist"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const body = {};
  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body)
  };
  return await fecth(url, options);
}
