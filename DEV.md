# Desarrollo

```bash
make pack
make publish
```

Los archivos que se van a subir en la publicación se encuentran dentro del ./package.json:

```bash
  "main": "nodemcu/nodemcu.es.js",
  "files": ["nodemcu"]
```

Ignorar los archivos generados en el ./.gitignore:

```bash
nodemcu.zip
nodemcu/
```
